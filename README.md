**Color based object filtering using PCL, OpenCV and OpenNI**
======================
Authors: Tian Zhao
Contacct: tianzhao at stanford.edu

# Demo: https://www.youtube.com/watch?v=N04Yw7hwbho&feature=youtu.be

# About: 

This is the first milestone of the summer research project. It uses OpenNI2 and OpenCV3 to detect an object selected by user. User first choose the object by right mouse-clicking on an object in the displayed video. This project then uses the color at the mouse-click to filter out the object in the video. This project then displays the point cloud of the specified object. 

# To build: 
mkdir build
cd ./build
cmake ..
// This will build the project in the build folder.
make 


## Problems to be solved: 
### PCL library can create point cloud at a satisfying rate. However the CloudViewer sometimes would update at only 6~10 Hz. From the PCL forum it seems that some other people have encountered the same problem. In the future, I plan to replace the CloudViewer with some other better supported tools, e.g. ROS, PCL Visualizer. 

### Video filtering: it seems that filtering out an object using a given color can behave quite unstably. The property of light in the environment is a huge factor, and I have not figured out an effective way to remove this impact. Currently, the workaround I have is to allow the users to adjust the saturation. As shown on the video, moving around the saturation bar can greatly affect the accuracy of object tracking.  